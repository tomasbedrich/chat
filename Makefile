# compiler and linker
CXX = g++
CXXFLAGS = -Wall -pedantic -Wno-long-long -O0 -ggdb -std=c++11 -I $(SRCDIR)
LDLIBS = -pthread -lncurses

# input and output
DOCDIR = doc
SRCDIR = src
DISTDIR = dist
OBJDIR = $(DISTDIR)/objects

# BIN = $(DISTDIR)/bedritom
BIN = ./bedritom
OBJECTS = \
  $(OBJDIR)/controllers/client.o \
  $(OBJDIR)/controllers/room.o \
  $(OBJDIR)/controllers/server.o \
  $(OBJDIR)/logger.o \
  $(OBJDIR)/main.o \
  $(OBJDIR)/models/client.o \
  $(OBJDIR)/models/room.o \
  $(OBJDIR)/models/server.o \
  $(OBJDIR)/tcp.o \
  $(OBJDIR)/views/client.o \
  $(OBJDIR)/views/helpers.o \
  $(OBJDIR)/views/room.o

# ==========================================================

# compile and create docs
# all: compile docs
all: compile run

# compile all
compile: createdirs $(BIN)

# create output dirs
createdirs:
	for d in "" "models" "controllers" "views"; do mkdir -p dist/objects/$$d; done

# run
run: compile
	./$(BIN)

# generate documentation
doc:
	doxygen > /dev/null

# remove generated files
clean:
	rm -rf $(DISTDIR) $(DOCDIR) $(BIN)

# link objects
$(BIN): $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $(BIN) $(OBJECTS) $(LDLIBS)

# ==========================================================
# individual targets

$(OBJDIR)/main.o: $(SRCDIR)/main.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/logger.o: $(SRCDIR)/logger.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/tcp.o: $(SRCDIR)/tcp.cpp $(SRCDIR)/tcp.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/controllers/client.o: $(SRCDIR)/controllers/client.cpp $(SRCDIR)/controllers/client.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/controllers/room.o: $(SRCDIR)/controllers/room.cpp $(SRCDIR)/controllers/room.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/controllers/server.o: $(SRCDIR)/controllers/server.cpp $(SRCDIR)/controllers/server.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/models/client.o: $(SRCDIR)/models/client.cpp $(SRCDIR)/models/client.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/models/room.o: $(SRCDIR)/models/room.cpp $(SRCDIR)/models/room.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/models/server.o: $(SRCDIR)/models/server.cpp $(SRCDIR)/models/server.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/views/client.o: $(SRCDIR)/views/client.cpp $(SRCDIR)/views/client.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/views/helpers.o: $(SRCDIR)/views/helpers.cpp $(SRCDIR)/views/helpers.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)/views/room.o: $(SRCDIR)/views/room.cpp $(SRCDIR)/views/room.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
