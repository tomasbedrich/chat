/**
 * PARTIALLY DOWLOADED CODE:
 * ======================================
 * This file is assembled from classes avaliable at:
 * https://github.com/vichargrave/tcpsockets
 * It uses about 250 lines of original code which was
 * slightly edited to better fit needs of this project.
 *
 *
 * Original license follows:
 * ======================================
 * Copyright 2013 [Vic Hargrave - http://vichargrave.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __tcp_h__
#define __tcp_h__

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <sstream>
#include <unistd.h>
#include <cstring>

#define BUFF_LEN 256

using namespace std;


/**
 * @brief Holds TCP/IP related classes.
 */
namespace TCP {

/**
 * @brief Raised when connection is closed unexpectly.
 */
class PeerDisconnectedException : public exception {
  public:
    /**
     * @copydoc exception::what()
     */
    virtual char const * what() const throw ();
};


/**
 * @brief Raised when operation timed out.
 */
class TimeOutException : public exception {
  public:
    /**
     * @copydoc exception::what()
     */
    virtual char const * what() const throw ();
};


/**
 * @brief Provides methods to transfer data between peers.
 */
class Stream {
  private:
    string m_peerIP;
    int m_sd, m_peerPort;

    Stream(int sd, struct sockaddr_in * address);

    Stream();

    Stream(Stream const & stream);

    bool waitForReadEvent(int timeout);

  public:
    friend class Acceptor;
    friend class Connector;

    enum {
        connectionClosed = 0,
        connectionReset = -1,
        connectionTimedOut = -2
    };

    ~Stream();

    /**
     * @brief Sends data through the stream.
     * @param buffer data to send
     * @param len data len
     * @return number of bytes written to stream
     */
    ssize_t send(char const * buffer, size_t len);

    /**
     * @author Tomas Bedrich
     * @copybrief send(const char *, size_t)
     * @param data data to send
     */
    void send(string const & data);

    /**
     * @brief Receive data from stream.
     * @details Blocks and waits for a data.
     * @param buffer where to store data
     * @param len buffer len
     * @param timeout read timeout
     * @return positive: number of bytes read, negative: error code
     */
    ssize_t receive(char * buffer, size_t len, int timeout = 0);

    /**
     * @author Tomas Bedrich
     * @copybrief receive(char *, size_t, int)
     * @param timeout read timeout
     * @return data received from peer
     * @throws PeerDisconnectedException when peer disconnects during reading data
     */
    string const receive(int timeout = 0);

    /**
     * @author Tomas Bedrich
     * @copybrief receive(char *, size_t, int)
     * @param exit flag to stop blocking
     * @param timeout how often to check exit flag
     * @throws PeerDisconnectedException in case of error
     * @return received message or "" when interrupted by exit flag
     * This method "blocks" while exit flag is set to false.
     */
    string receive(bool * exit, int timeout = 1);

    /// @return IP address of a peer
    inline string getPeerIP() const {
        return m_peerIP;
    }

    /// @return port of a peer
    inline int getPeerPort() const {
        return m_peerPort;
    }

    /// @return IP address and port of a peer
    inline string getPeerID() const {
        stringstream res;
        res << m_peerIP << ":" << m_peerPort;
        return res.str();
    }
};


/**
 * @brief Provides methods to passively establish connections.
 */
class Acceptor {
  private:
    string m_address;
    int m_lsd, m_port;
    bool m_listening;

    Acceptor();

  public:
    /**
     * @brief Creates an instance.
     * @param port listen port
     * @param address listen address (empty string means any address/interface)
     */
    Acceptor(int port, char const * address = "");

    ~Acceptor();

    /**
     * @brief Creates a server socket.
     * @param qlen maximum client queue lenght
     * @return operation exit code
     */
    int start(unsigned char qlen = 5);

    /**
     * @brief Accepts a client on the socket.
     * @details Blocks and waits for a client.
     * @return Stream to client or NULL
     */
    Stream * accept();
};


/**
 * @brief Provides methods to actively establish connections.
 */
class Connector {
  private:
    int resolveHostName(char const * hostname, struct in_addr * addr);

  public:
    /**
     * @brief Creates a connection with server.
     * @param server server IP or hostname
     * @param port server port
     * @return a Stream with estabilished connection or NULL
     */
    Stream * connect(char const * server, int port);
};

} // end namespace TCP

#endif
