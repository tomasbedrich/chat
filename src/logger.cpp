#ifndef __logger__
#define __logger__

#include <iostream>
#include <sstream>
#include <time.h>

using namespace std;

/**
 * @brief Basic console logger.
 *
 * Example usage:
 *
 *     Log::info() << "message";
 */
class Log {
  private:
    Log() {}
    Log(const Log & other) {}
    void operator=(const Log & other) {}

  public:
    /**
     * @brief Base of all output loggeres.
     */
    struct log {
      private:
        ostringstream m_data;
        string m_level;
        bool m_flush;

      public:
        /**
         * @brief Creates an output logger.
         * @param level short string identifying level
         * @param flush whether to flush or discard an output
         */
        log(const string & level, bool flush = true) : m_level(level), m_flush(flush) {}

        /**
         * @brief Flushes output.
         */
        ~log() {
            if (!m_flush) return;
            cerr << "(" << clock() << ") " << m_level << ": " << m_data.str() << endl;
        }

        /**
         * @brief Adds data to output.
         * @param data any printable object
         */
        template <typename T>
        log & operator<<(const T & data) {
            m_data << data;
            return *this;
        }
    };

    /**
     * @brief Debug message logger
     */
    struct debug : log {
        // disable debug logging
        debug() : log("DEBUG", false) {}
    };

    /**
     * @brief Info message logger
     */
    struct info : log {
        info() : log("INFO") {}
    };

    /**
     * @brief Warning message logger
     */
    struct warning : log {
        warning() : log("WARNING") {}
    };

    /**
     * @brief Error message logger
     */
    struct error : log {
        error() : log("ERROR") {}
    };
};

#endif
