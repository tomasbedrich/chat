#include "controllers/controller.h"
#include "controllers/server.h"
#include "controllers/client.h"

using namespace std;

void print_help(char const * executable) {
    cout << "===========================================================" << endl;
    cout << "USAGE:  " << executable << "  mode  port  [host]" << endl;
    cout << "-----------------------------------------------------------" << endl;
    cout << "mode:   [ server | client ]" << endl;
    cout << "port:   <1025-65535>" << endl;
    cout << "host:   hostname or IP (optional in server mode)" << endl;
    cout << "===========================================================" << endl;
}

int main(int argc, char const * argv[]) {

    // parse args
    Controller * main_controller;

    try {

        // not enough arguments => exception
        if (argc < 3) throw NULL;

        // parse mode
        string mode = string(argv[1]);

        // not server neither client => exception
        if (mode.compare("server") && mode.compare("client")) throw NULL;

        // try to parse port (cannot convert port => exception)
        int port = stoi(argv[2]);

        // invalid port => exception
        if (port < 1025 || port > 65535) throw NULL;

        // client mode
        if (mode.compare("client") == 0) {

            // try to parse host (no host => exception)
            if (argc < 4) throw NULL;
            string host = string(argv[3]);
            if (host.empty()) throw NULL;

            main_controller = new ClientController(port, host);
        }

        // server mode
        else {
            string host = argc < 4 ? "" : string(argv[3]);
            main_controller = new ServerController(port, host);
        }

    }

    // fallback => print help and exit
    catch (...) {
        print_help(argv[0]);
        // return EXIT_FAILURE;
        return EXIT_SUCCESS;
    }

    // run, cleanup and exit
    main_controller->loop();
    delete main_controller;
    return EXIT_SUCCESS;
}
