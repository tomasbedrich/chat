#include "tcp.h"

using namespace std;

namespace TCP {

char const * PeerDisconnectedException::what() const throw () {
    return "peer unexpectly disconnected";
}

char const * TimeOutException::what() const throw () {
    return "operation timeout";
}

Stream::Stream(int sd, struct sockaddr_in * address)
    : m_sd(sd) {
    char ip[50];
    inet_ntop(PF_INET, (struct in_addr *) & (address->sin_addr.s_addr), ip, sizeof(ip) - 1);
    m_peerIP = ip;
    m_peerPort = ntohs(address->sin_port);
}

bool Stream::waitForReadEvent(int timeout) {
    fd_set sdset;
    struct timeval tv;

    tv.tv_sec = timeout;
    tv.tv_usec = 0;
    FD_ZERO(&sdset);
    FD_SET(m_sd, &sdset);
    return select(m_sd + 1, &sdset, NULL, NULL, &tv) > 0 ? true : false;
}

Stream::~Stream() {
    close(m_sd);
}

ssize_t Stream::send(char const * buffer, size_t len) {
    return write(m_sd, buffer, len);
}

void Stream::send(string const & data) {
    send(data.c_str(), data.size());
}

ssize_t Stream::receive(char * buffer, size_t len, int timeout) {
    if (timeout <= 0) return read(m_sd, buffer, len);

    if (waitForReadEvent(timeout) == true)
        return read(m_sd, buffer, len);
    return connectionTimedOut;
}

string const Stream::receive(int timeout) {
    char buf[BUFF_LEN];
    ssize_t len;
    stringstream res;
    while ((len = receive(buf, BUFF_LEN, timeout)) > 0) {
        buf[len] = 0;
        res << buf;
        if (len < BUFF_LEN)
            return res.str();
    }
    if (len == connectionTimedOut)
        throw TimeOutException();
    else throw PeerDisconnectedException();
}

string Stream::receive(bool * exit, int timeout) {
    while (!*exit) {
        try {
            return receive(timeout);
        } catch (TCP::TimeOutException & e) {
            continue;
        }
    }
    return "";
}

Acceptor::Acceptor() {
}

Acceptor::Acceptor(int port, char const * address) : m_address(address), m_lsd(0), m_port(port), m_listening(false) {
}

Acceptor::~Acceptor() {
    if (m_lsd > 0)
        close(m_lsd);
}

int Acceptor::start(unsigned char qlen) {
    if (m_listening == true) return 0;

    m_lsd = socket(PF_INET, SOCK_STREAM, 0);
    struct sockaddr_in address;

    memset(&address, 0, sizeof(address));
    address.sin_family = PF_INET;
    address.sin_port = htons(m_port);
    if (m_address.size() > 0) {
        inet_pton(PF_INET, m_address.c_str(), &(address.sin_addr));
    } else {
        address.sin_addr.s_addr = INADDR_ANY;
    }

    int optval = 1;
    setsockopt(m_lsd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

    int result = ::bind(m_lsd, (struct sockaddr *)&address, sizeof(address));
    if (result != 0) return result;

    result = listen(m_lsd, qlen);
    if (result != 0) return result;
    m_listening = true;
    return result;
}

Stream * Acceptor::accept() {
    if (m_listening == false) return NULL;

    struct sockaddr_in address;
    socklen_t len = sizeof(address);
    memset(&address, 0, sizeof(address));
    int sd = ::accept(m_lsd, (struct sockaddr *)&address, &len);
    if (sd < 0) return NULL;
    return new Stream(sd, &address);
}

Stream * Connector::connect(char const * server, int port) {
    struct sockaddr_in address;

    memset(&address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    if (resolveHostName(server, &(address.sin_addr)) != 0) {
        inet_pton(PF_INET, server, &(address.sin_addr));
    }
    int sd = socket(AF_INET, SOCK_STREAM, 0);
    if (::connect(sd, (struct sockaddr *)&address, sizeof(address)) != 0) return NULL;
    return new Stream(sd, &address);
}

int Connector::resolveHostName(char const * hostname, struct in_addr * addr) {
    struct addrinfo * res;

    int result = getaddrinfo(hostname, NULL, NULL, &res);
    if (result == 0) {
        memcpy(addr, &((struct sockaddr_in *) res->ai_addr)->sin_addr, sizeof(struct in_addr));
        freeaddrinfo(res);
    }
    return result;
}

} // end namespace TCP
