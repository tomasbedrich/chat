#ifndef __helpers_view_h__
#define __helpers_view_h__

#include <ncurses.h>
#include <string>
#include <list>
#include <cstdlib>

using namespace std;


/**
 * @brief Holds classes and helpers related to GUI.
 */
namespace GUI {

// graphic styles
static chtype const ST_BOLD_RED = A_BOLD | COLOR_PAIR(1);
static chtype const ST_INVERSE = A_REVERSE;
static chtype const ST_BLUE = COLOR_PAIR(3);

/**
 * @brief Init curses and colors. (only once)
 */
void ensure_curses();

/**
 * @brief Prints styled text.
 * @param win target window
 * @param y x position
 * @param x y position
 * @param text text
 * @param style style
 */
void mvwprintw_styled(WINDOW * win, int y, int x, char const * text, chtype style);

/**
 * @brief Reads input from keyboard.
 * @param win window for displaying input "field"
 * @param y starting y position
 * @param x starting x position
 * @param max_len maximum input length (default is screen width)
 * @return typed text
 */
string input(WINDOW * win, int y, int x, int max_len);

/**
 * @brief Prompts user to input a text.
 * @param msg prompt message
 * @return typed text
 */
string prompt(string const & msg);

/**
 * @brief Shows an alert.
 * @param msg alert message
 */
void alert(string const & msg);

/**
 * @brief Represents a menu.
 *
 * Lets user select one option and returns its index in list.
 *
 * Example usage:
 *
 *     list<string> l = list<string>();
 *     l.push_back("one");
 *     l.push_back("two");
 *     l.push_back("three");
 *     int choice = Menu(l).show();
 */
class Menu {
  private:
    WINDOW * m_win;
    list <string> const & m_items;
    char const * m_title;
    int m_active, m_inner_width, m_inner_height;

    void redraw() const;

  public:
    /**
     * @brief Creates a menu.
     * @param items list containing menu items
     * @param title menu title
     * @param width width in cols (default is almost whole screen)
     * @param height height in rows (default is almost whole screen)
     */
    Menu(list <string> const & items, string const & title = " CHOOSE PLEASE ", size_t width = 0, size_t height = 0);

    ~Menu();

    /**
     * @brief Shows a menu.
     * @return selected item index
     */
    int show();
};

} // end namespace GUI

#endif
