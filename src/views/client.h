#ifndef __client_view_h__
#define __client_view_h__

#include <ncurses.h>
#include <list>
#include <string>

#include "views/helpers.h"
#include "models/client.h"


/**
 * @brief Handles client mode menus and submenus, error messages, prompts.
 */
class ClientView {
  private:
    ClientModel & m_model;

  public:
    /**
     * @brief Creates main client GUI.
     * @param model reference to model
     */
    ClientView(ClientModel & model);

    /**
     * @brief Displays main menu.
     * @return selected item's index
     */
    int main_menu() const;

    /**
     * @brief Displays connected menu.
     * @return selected item's index
     */
    int connected_menu() const;

    /**
     * @brief Displays configuration menu.
     * @return selected item's index
     */
    int settings_menu() const;

    /**
     * @brief Displays room selection menu.
     * @return selected item's index
     */
    int select_room(list <string> & rooms) const;

    /**
     * @brief Prompts user for a room name.
     * @return room name
     */
    string room_name_prompt() const;

    /**
     * @brief Prompts user for a new username.
     * @return username
     */
    string name_prompt() const;

    /**
     * @brief Prompts user for a server host.
     * @return host
     */
    string host_prompt() const;

    /**
     * @brief Prompts user for a server port.
     * @return port
     */
    int port_prompt() const;

    /**
     * @brief Displays about dialog.
     */
    void about() const;

    /**
     * @brief Displays connection error alert.
     */
    void connection_error_alert() const;

    /**
     * @brief Displays PeerDisconnectedException alert.
     */
    void connection_closed_alert() const;
};

#endif
