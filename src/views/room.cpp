#include "room.h"

using namespace std;

RoomView::RoomView(RoomModel & model)
    : m_model(model) {
    GUI::ensure_curses();
    clear();

    getmaxyx(stdscr, m_rows, m_cols);
    m_inner_height = m_rows - 5;
    m_win = newwin(m_inner_height, m_cols, 2, 0);

    // room title
    string title = " ROOM: " + m_model.get_name();
    title.resize(m_cols, ' ');
    GUI::mvwprintw_styled(stdscr, 0, 0, title.c_str(), GUI::ST_BOLD_RED | GUI::ST_INVERSE);

    // toolbar
    string toolbar_left = " type a message:";
    string toolbar_right = "or '/exit' to leave a room ";
    toolbar_left.resize(m_cols - toolbar_right.size(), ' ');
    toolbar_left += toolbar_right;
    GUI::mvwprintw_styled(stdscr, m_rows - 2, 0, toolbar_left.c_str(), GUI::ST_BLUE | GUI::ST_INVERSE);

    refresh();
}

RoomView::~RoomView() {
    delwin(m_win);
    clear();
    refresh();
}

void RoomView::redraw() const {
    int row = 0;
    for (const string & message : m_model.get_messages()) {

        wclrtobot(m_win);
        mvwprintw(m_win, row, 0, message.c_str());

        if (row++ > m_inner_height) break; // bottom padding
    }

    wrefresh(m_win);
}

string RoomView::input() const {
    return GUI::input(stdscr, m_rows - 1, 0, m_cols);
}
