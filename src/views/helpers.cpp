#include "helpers.h"

using namespace std;

namespace GUI {

void ensure_curses() {
    static bool curses_setup_done = false;
    if (curses_setup_done) return;

    // setup curses
    initscr();
    noecho();
    curs_set(0);
    clear();
    refresh();

    // colors
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_BLUE, COLOR_BLACK);

    atexit((void (*)())endwin);

    curses_setup_done = true;
}

void mvwprintw_styled(WINDOW * win, int y, int x, char const * text, chtype style) {
    wattron(win, style);
    mvwprintw(win, y, x, "%s", text);
    wattroff(win, style);
}

string input(WINDOW * win, int y, int x, int max_len) {
    char * str = new char[max_len + 1];
    echo();
    mvwgetnstr(win, y, x, str, max_len);
    noecho();
    wclrtobot(win);
    string res = str;
    delete[] str;
    return res;
}

string prompt(string const & msg) {
    int rows, cols;
    getmaxyx(stdscr, rows, cols);

    // window and box
    int height = 4, width = cols - 8;
    WINDOW * win = newwin(height, width, (rows - height) / 2, (cols - width) / 2);
    box(win, 0, 0);

    // title and prompt
    mvwprintw_styled(win, 1, 2, msg.c_str(), ST_BOLD_RED);
    wrefresh(win);
    // TODO variable input length (max_len)
    string res = input(win, 2, 2, width - 4);

    delwin(win);
    clear();
    refresh();
    return res;
}

void alert(string const & msg) {
    int rows, cols;
    getmaxyx(stdscr, rows, cols);

    // window and box
    int height = 3, width = cols - 8;
    WINDOW * win = newwin(height, width, (rows - height) / 2, (cols - width) / 2);
    box(win, 0, 0);

    // title and prompt
    mvwprintw_styled(win, 1, 2, msg.c_str(), ST_BOLD_RED);
    mvwprintw_styled(win, 2, width - 8, "  OK  ", ST_INVERSE);
    wrefresh(win);
    while (getch() != 10);

    delwin(win);
    clear();
    refresh();
}

void Menu::redraw() const {
    int col = 1, row = 1, i = 0;
    int offset = max(0, m_active - m_inner_height + 1);

    // box and title
    box(m_win, 0, 0);
    mvwprintw_styled(m_win, 0, 2, m_title, ST_BOLD_RED);

    for (string item : m_items) {

        // scrolling
        if (i++ < offset) continue;

        // resize string (fill with spaces) so active item has full width
        item = " " + item;
        item.resize(m_inner_width, ' ');
        const char * str = item.c_str();

        // show item
        if (m_active == i - 1) mvwprintw_styled(m_win, row, col, str, ST_INVERSE);
        else mvwprintw(m_win, row, col, str);

        row++;
        if (row > m_inner_height) {
            if (m_items.size() - m_inner_height - offset)
                mvwprintw_styled(m_win, m_inner_height + 1, m_inner_width - 7, " [more] ", ST_BLUE);
            break;
        }
    }
    wrefresh(m_win);
}

Menu::Menu(list <string> const & items, string const & title, size_t width, size_t height)
    : m_items(items), m_title(title.c_str()), m_active(0) {

    ensure_curses();

    int rows, cols;
    getmaxyx(stdscr, rows, cols);

    // calculate W x H
    if (width <= 2) width = cols - 20; // some horizontal padding
    if (height < 4) height = rows - 4; // 2 items and 2 border
    height = max((int) m_items.size() + 2, (int) height);
    height = min(rows, (int) height);

    // calculate inner W x H
    m_inner_width = width - 2;
    m_inner_height = height - 2;

    // init win
    m_win = newwin(height, width, (rows - height) / 2, (cols - width) / 2);
    keypad(m_win, true);
    clear();
    refresh();
}

Menu::~Menu() {
    keypad(m_win, false);
    delwin(m_win);
    clear();
    refresh();
}

int Menu::show() {
    while (true) {
        redraw();
        // keyboard polling
        switch (wgetch(m_win)) {
        case KEY_UP: m_active = m_active ? m_active - 1 : m_items.size() - 1; break;
        case KEY_DOWN: m_active = (m_active == (int) m_items.size() - 1) ? 0 : m_active + 1; break;
        case 10: return m_active; // enter
        case 27: return -1; // escape
        }
    }
}

} // end namespace GUI
