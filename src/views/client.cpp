#include "client.h"

using namespace std;

ClientView::ClientView(ClientModel & model) : m_model(model) {
}

int ClientView::main_menu() const {
    list<string> items;
    items.push_back("connect");
    items.push_back("settings");
    items.push_back("about");
    items.push_back("exit");
    return GUI::Menu(items, " MAIN MENU ").show();
}

int ClientView::connected_menu() const {
    list<string> items;
    items.push_back("list rooms");
    items.push_back("disconnect");
    return GUI::Menu(items, " SERVER: " + m_model.get_host() + " ").show();
}

int ClientView::settings_menu() const {
    list<string> items;
    items.push_back("name: " + m_model.get_name() + " (select to change)");
    items.push_back("server: " + m_model.get_host() + " (select to change)");
    ostringstream port;
    port << m_model.get_port();
    items.push_back("port: " + port.str() + " (select to change)");
    items.push_back("back");
    return GUI::Menu(items, " SETTINGS ").show();
}

int ClientView::select_room(list <string> & rooms) const {
    rooms.push_front("<create new>");
    return GUI::Menu(rooms, " AVALIABLE ROOMS ").show();
}

string ClientView::room_name_prompt() const {
    string name, prompt = "Please enter a name for the room:";
    while (true) {
        name = GUI::prompt(prompt);
        // validation
        if (name.find("<create new>") == 0 || name.empty()) {
            prompt = "... this name cannot be used!";
            continue;
        }
        return name;
    }
}

string ClientView::name_prompt() const {
    return GUI::prompt("Please enter a new username:");
}

string ClientView::host_prompt() const {
    return GUI::prompt("Please enter a server IP or hostname:");
}

int ClientView::port_prompt() const {
    string port, prompt = "Please enter a new port (1025 - 65535):";
    while (true) {
        port = GUI::prompt(prompt);
        // validation
        try {
            int res = stoi(port);
            if (res < 1025 || res > 65535) throw NULL;
            return res;
        } catch (...) {
            prompt = "... this port cannot be used!";
        }
    }
}

void ClientView::about() const {
    GUI::alert("Copyright 2014, Tomas Bedrich (ja@tbedrich.cz)");
}

void ClientView::connection_error_alert() const {
    GUI::alert("Cannot connect to " + m_model.get_id());
}

void ClientView::connection_closed_alert() const {
    GUI::alert("Server unexpectly closed the connection.");
}
