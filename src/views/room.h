#ifndef __room_view_h__
#define __room_view_h__

#include <ncurses.h>
#include <list>
#include <string>

#include "views/helpers.h"
#include "models/room.h"


/**
 * @brief Draws a chat room and handles input
 */
class RoomView {
  private:
    RoomModel & m_model;
    WINDOW * m_win;
    int m_rows, m_cols, m_inner_height;

  public:
    /**
     * @param model data source
     */
    RoomView(RoomModel & model);

    ~RoomView();

    /**
     * @brief Redraws screen.
     */
    void redraw() const;

    /**
     * @brief Returns user input.
     */
    string input() const;
};

#endif
