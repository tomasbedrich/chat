#ifndef __server_model_h__
#define __server_model_h__

#include <string>
#include <map>
#include <list>

#include "tcp.h"

/**
 * @brief Holds chat rooms - its names and subscribers (clients).
 */
class ServerModel {
  private:
    map <string, list<TCP::Stream *> > m_rooms;
    string const & m_host;
    int m_port;

  public:
    /**
     * @param port listen port
     * @param host address (network interface) to use
     */
    ServerModel(int port, string const & host = "");

    ~ServerModel();

    /**
     * @return list of rooms separated by newlines
     */
    string list_rooms() const;

    /**
     * @param room room name
     * @return list of client streams subscribed to the room
     */
    list <TCP::Stream *> list_clients(string const & room);

    /**
     * @brief Subscribes a client to a room.
     * @param room room name
     * @param stream client stream
     */
    void add_subscriber(string const & room, TCP::Stream * stream);

    /**
     * @brief Removes a client and deletes a room if empty.
     * @param room room name
     * @param stream client stream
     */
    void remove_subscriber(string const & room, TCP::Stream * stream);

    /// @return host address
    inline string const & get_host() const {
        return m_host;
    }

    /// @return listen port
    inline int get_port() const {
        return m_port;
    }
};

#endif
