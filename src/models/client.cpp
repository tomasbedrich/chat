#include "client.h"

using namespace std;

void ClientModel::send_command(string const & command) {
    if (!m_stream) throw TCP::PeerDisconnectedException();
    m_stream->send(command);
}

ClientModel::ClientModel(int port, string host, string name)
    : m_name(name), m_stream(NULL), m_host(host), m_port(port) {
}

ClientModel::~ClientModel() {
    disconnect();
}

bool ClientModel::connect() {
    if (m_host.empty() || !m_port) return false;
    TCP::Connector connector;
    m_stream = connector.connect(m_host.c_str(), m_port);
    return m_stream;
}

void ClientModel::disconnect() {
    if (m_stream) {
        send_command("disconnect");
        delete m_stream;
        m_stream = NULL;
    }
}

list <string> ClientModel::list_rooms() {
    list<string> res;
    send_command("list");
    stringstream raw(m_stream->receive());
    string line;
    while (getline(raw, line)) {
        if (line.empty()) continue;
        res.push_back(line);
    }
    return res;
}

void ClientModel::join(string const & room) {
    send_command("join " + room);
}

void ClientModel::leave() {
    send_command("leave");
}

void ClientModel::send(string const & message) {
    send_command("send " + m_name + ": " + message);
}

string ClientModel::receive(bool * exit) {
    try {
        return m_stream->receive(exit);
    } catch (TCP::PeerDisconnectedException & e) {
        delete m_stream;
        m_stream = NULL;
        throw;
    }
}
