#include "server.h"

using namespace std;

ServerModel::ServerModel(int port, string const & host) : m_host(host), m_port(port) {
}

ServerModel::~ServerModel() {
}

string ServerModel::list_rooms() const {
    stringstream buffer;
    bool first = true;
    for (const auto & item : m_rooms) {
        if (!first) buffer << "\n";
        buffer << item.first;
        first = false;
    }
    if (first) buffer << "\n";
    return buffer.str();
}

list <TCP::Stream *> ServerModel::list_clients(string const & room) {
    return m_rooms[room];
}

void ServerModel::add_subscriber(string const & room, TCP::Stream * stream) {
    m_rooms[room].push_back(stream);
}

void ServerModel::remove_subscriber(string const & room, TCP::Stream * stream) {
    m_rooms[room].remove(stream);
    if (!m_rooms[room].size()) m_rooms.erase(room);
}
