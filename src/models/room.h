#ifndef __room_model_h__
#define __room_model_h__

#include <string>
#include <deque>

#include "tcp.h"


/**
 * @brief Hold room name and messages
 */
class RoomModel {
  private:
    string const & m_name;
    deque <string> m_messages;
    unsigned int m_capacity;

    // disable copies
    RoomModel(RoomModel const & other);

  public:
    /**
     * @param name room name
     * @param capacity message queue capacity
     */
    RoomModel(string const & name, unsigned int capacity = 20);
    
    ~RoomModel();

    /**
     * @brief Adds a message to the queue.
     * @param text message text
     */
    void append_message(string const & text);

    /// @return messages
    deque <string> const get_messages() {
        return m_messages;
    }

    /// @return room name
    string get_name() const {
        return m_name;
    }
};

#endif
