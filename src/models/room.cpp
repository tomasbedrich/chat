#include "room.h"

using namespace std;

RoomModel::RoomModel(string const & name, unsigned int capacity)
    : m_name(name), m_capacity(capacity) {
}

RoomModel::~RoomModel() {
}

void RoomModel::append_message(string const & text) {
    m_messages.push_front(text);
    // remove old ones
    if (m_messages.size() > m_capacity)
        m_messages.pop_back();
}