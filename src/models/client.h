#ifndef __client_model_h__
#define __client_model_h__

#include <string>
#include <list>

#include "tcp.h"


/**
 * @brief Holds client name, TCP::Stream and server socket.
 */
class ClientModel {
  private:
    string m_name;
    TCP::Stream * m_stream;
    string m_host;
    int m_port;

    /**
     * @brief Sends a command to server.
     * @param command the command to send
     */
    void send_command(string const & command);

    // disable copies
    ClientModel(ClientModel const & other);

  public:
    /**
     * @param port server port
     * @param host server IP or hostname
     * @param name client name (to be used in conversations)
     */
    ClientModel(int port = 0, string host = "", string name = "Anonymous");

    ~ClientModel();


    // "NETWORK" MODEL ==================================================

    /**
     * @brief Establish connection to server.
     * @return true in case of successful connection
     */
    bool connect();

    /**
     * @brief Closes connection with server (if any).
     */
    void disconnect();

    /**
     * @brief Lists rooms avaliable on server.
     * @return list of avaliable rooms
     */
    list <string> list_rooms();

    /**
     * @brief Connects to a room.
     * @param room where to connect
     */
    void join(string const & room);

    /**
     * @brief Leaves current room.
     */
    void leave();

    /**
     * @brief Sends a message to current room.
     * @param message a message to send
     */
    void send(string const & message);

    /**
     * @copybrief TCP::Stream::receive(bool *, int)
     * @param exit flag to stop blocking
     */
    string receive(bool * exit);


    // GETTERS + SETTERS ================================================

    /// @return client name
    inline string get_name() const {
        return m_name;
    }

    /// @param name new client's name
    inline void set_name(string name) {
        m_name = name;
    }

    /// @return wheter the connection is established
    inline bool is_connected() const {
        return m_stream;
    }

    /// @param stream new TCP::Stream associated with client
    inline void set_stream(TCP::Stream * stream) {
        m_stream = stream;
    }
    
    /// @return server address
    inline string get_host() const {
        return m_host;
    }

    /// @return server listen port
    inline int get_port() const {
        return m_port;
    }

    /// @return server id (host:port)
    inline string get_id() const {
        stringstream res;
        res << m_host << ":" << m_port;
        return res.str();
    }
    
    /// @param host new server address
    inline void set_host(string host) {
        m_host = host;
    }

    /// @param port new server port
    inline void set_port(int port) {
        m_port = port;
    }
};

#endif