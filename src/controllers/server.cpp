#include "server.h"

using namespace std;

void ServerController::client_handler(TCP::Stream * stream) {
    // active room, last command
    string room, command;
    Log::info() << "client " << stream->getPeerID() << " connected";

    try {
        while (true) {

            // read command
            command = stream->receive();
            Log::debug() << "client " << stream->getPeerID() << " command: " << command;

            // do action
            if (command.find("disconnect") == 0) {
                break;

            } else if (command.find("list") == 0) {
                stream->send(m_model->list_rooms());

            } else if (command.find("join") == 0) {
                room = command.substr(5);
                m_model->add_subscriber(room, stream);

            } else if (command.find("leave") == 0) {
                m_model->remove_subscriber(room, stream);

            } else if (command.find("send") == 0) {
                string message = command.substr(5);
                for (TCP::Stream * peer : m_model->list_clients(room))
                    peer->send(message);

            } else {
                Log::warning() << "unknown command, ignoring";
            }
        }
    } catch (TCP::PeerDisconnectedException & e) {
        Log::warning() << "client " << stream->getPeerID() << " " << e.what();
    }

    // do some cleanup
    Log::info() << "client " << stream->getPeerID() << " disconnect";
    if (!room.empty()) m_model->remove_subscriber(room, stream);
    delete stream;
}

ServerController::ServerController(int port, string const & host) {
    m_model = new ServerModel(port, host);
}

ServerController::~ServerController() {
    delete m_model;
}

void ServerController::loop() {
    // start listening
    TCP::Acceptor acceptor(m_model->get_port(), m_model->get_host().c_str());
    if (acceptor.start() != 0) {
        Log::error() << "cannot start server (already running?)";
        return;
    }
    Log::info() << "server started, waiting for clients";

    // create thread pool
    list<thread> pool;

    while (true) {
        // accept clients (blocking)
        TCP::Stream * stream = acceptor.accept();
        if (stream == NULL) {
            Log::error() << "client accept failed";
            break;
        }
        // add to thread pool
        pool.push_back(thread(&ServerController::client_handler, this, stream));
    }

    Log::debug() << "main thread is ending, joining others";
    for (thread & t : pool) t.join();
}
