#ifndef __room_controller_h__
#define __room_controller_h__

#include <thread>

#include "controllers/controller.h"
#include "logger.cpp"
#include "models/client.h"
#include "models/room.h"
#include "views/room.h"


/**
 * @brief Used to handle room related events.
 */
class RoomController : public Controller {
  private:
    RoomModel * m_model;
    RoomView * m_view;
    ClientModel * m_cli_model;
    bool * m_exit;

    /**
     * @brief A thread for handling user input.
     */
    void sending();

    /**
     * @brief A thread for handling server messages.
     */
    void receiving();

    // disable copies
    RoomController(RoomController const & other);

  public:
    /**
     * @param name room name
     * @param cli_model pointer to ClientModel (to use its TCP::Stream)
     */
    RoomController(string const & name, ClientModel * cli_model);

    virtual ~RoomController();

    /**
     * @brief Joins a room and executes threads.
     */
    void loop();
};

#endif
