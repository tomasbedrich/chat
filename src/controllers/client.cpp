#include "client.h"

using namespace std;

void ClientController::select_room() {
    string room_name;

    // show menu
    list<string> rooms = m_model->list_rooms();
    int choice = m_view->select_room(rooms);

    if (choice == 0) // create new room
        room_name = m_view->room_name_prompt();

    else if (choice == -1)
        return;

    else // connect to existing room
        room_name = *next(rooms.begin(), choice);

    // pass control
    RoomController(room_name, m_model).loop();
}

void ClientController::connected_loop() {
    while (true) {
        switch (m_view->connected_menu()) {
        // list rooms
        case 0:
            select_room();
            break;
        // disconnect
        default:
            m_model->disconnect();
            return;
        }
    }
}

void ClientController::settings_loop() {
    while (true) {
        switch (m_view->settings_menu()) {
        // name
        case 0:
            m_model->set_name(m_view->name_prompt());
            break;
        // host
        case 1:
            m_model->set_host(m_view->host_prompt());
            break;
        // port
        case 2:
            m_model->set_port(m_view->port_prompt());
            break;
        // exit
        default:
            return;
        }
    }
}

ClientController::ClientController(int port, string host) {
    m_model = new ClientModel(port, host);
    m_view = new ClientView(*m_model);
}

ClientController::~ClientController() {
    delete m_model;
    delete m_view;
}

void ClientController::loop() {
    while (true) {
        try {
            switch (m_view->main_menu()) {
            // connect
            case 0:
                if (m_model->connect()) connected_loop();
                else m_view->connection_error_alert();
                break;
            // settings
            case 1:
                settings_loop();
                break;
            // about
            case 2:
                m_view->about();
                break;
            // exit
            default:
                return;
            }
        } catch (TCP::PeerDisconnectedException & e) {
            m_view->connection_closed_alert();
        }
    }
}
