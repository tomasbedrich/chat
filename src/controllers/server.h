#ifndef __server_controller_h__
#define __server_controller_h__

#include <ncurses.h>
#include <string>
#include <map>
#include <list>
#include <thread>

#include "controllers/controller.h"
#include "models/server.h"
#include "logger.cpp"

/**
 * @brief Controls client accepting and threads.
 */
class ServerController : public Controller {
  private:
    ServerModel * m_model;

    /**
     * @brief Threaded handler of client's commands.
     * @param stream client stream
     */
    void client_handler(TCP::Stream * stream);

    // disable copies
    ServerController(ServerController const & other);

  public:
    /**
     * @param port listen port
     * @param host address (network interface) to use
     */
    ServerController(int port, string const & host = "");

    virtual ~ServerController();

    /**
     * @brief Accepts clients and spawns new thread for each.
     */
    void loop();
};

#endif
