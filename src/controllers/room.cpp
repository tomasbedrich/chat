#include "room.h"

using namespace std;

void RoomController::sending() {
    while (true) {
        string text = m_view->input();
        if (*m_exit || text == "/exit") break;
        if (text.empty()) continue;
        m_cli_model->send(text);
    }
    // shutdown
    *m_exit = true;
    Log::debug() << "sending thread stopped";
}

void RoomController::receiving() {
    try {
        while (true) {
            const string & text = m_cli_model->receive(m_exit);
            if (*m_exit) break;
            m_model->append_message(text);
            m_view->redraw();
        }
    } catch (TCP::PeerDisconnectedException & e) {
        // act as when client shutdown happened
    }
    // shutdown
    *m_exit = true;
    Log::debug() << "receiving thread stopped";
}

RoomController::RoomController(string const & name, ClientModel * cli_model)
    : m_cli_model(cli_model) {
    m_model = new RoomModel(name);
    m_view = new RoomView(*m_model);
    m_exit = new bool(false);
}

RoomController::~RoomController() {
    delete m_model;
    delete m_view;
    delete m_exit;
}

void RoomController::loop() {
    m_cli_model->join(m_model->get_name());
    m_view->redraw();
    thread receiver(&RoomController::receiving, this);
    sending();
    receiver.join();
    m_cli_model->leave();
}
