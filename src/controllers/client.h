#ifndef __client_controller_h__
#define __client_controller_h__

#include <list>

#include "controllers/controller.h"
#include "models/client.h"
#include "views/client.h"
#include "controllers/room.h"


/**
 * @brief Main controller for client mode.
 */
class ClientController : public Controller {
  private:
    ClientModel * m_model;
    ClientView * m_view;

    /**
     * @brief Handles room selection or new rooms creation.
     */
    void select_room();

    /**
     * @brief Runs helper GUI loop which is used in connected state.
     */
    void connected_loop();

    /**
     * @brief Runs helper GUI loop which in settings menu.
     */
    void settings_loop();

    // disable copies
    ClientController(ClientController const & other);

    /**
     * @brief Runs main GUI loop.
     * @details Handles user events and routes program execution.
     */
    void loop();

  public:
    /**
     * @param port server port
     * @param host server IP or hostname
     */
    ClientController(int port = 0, string host = "");

    virtual ~ClientController();
};

#endif
