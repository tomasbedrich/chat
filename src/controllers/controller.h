#ifndef __controller_h__
#define __controller_h__

/**
 * @brief Base for other controllers.
 */
class Controller {
  public:
    /**
     * @brief Controller's main loop.
     * 
     * It should be executed after controller creation.
     * When this happends, the controller gets a control
     * above the execution of the program.
     */
    virtual void loop() = 0;

    inline virtual ~Controller() {};
};

#endif
